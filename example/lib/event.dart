import 'package:like_sealed/like_sealed.dart';
import 'package:meta/meta.dart';

part 'event.sealed.dart';

@LikeSealed(switchImpl: true)
abstract class Event {}

class EventInit extends Event {}

class EventUpdate extends Event {}
