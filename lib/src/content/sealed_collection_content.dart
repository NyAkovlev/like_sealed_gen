import 'package:analyzer/dart/element/element.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:like_sealed_gen/src/description/class_description.dart';
import 'package:like_sealed_gen/src/util.dart';

class SealedCollectionContent {
  final LikeSealed config;
  final ClassDescription parentType;
  final List<ClassDescription> types;

  SealedCollectionContent(this.config,
      this.parentType,
      this.types,);

  String content() {
    return """
   class ${parentType.name}s {
   ${_methods()}
   }
   """;
  }

  String _methods() {
    final methods = <String>[];
    for (var type in types) {
      if (type.isAbstract) {
        continue;
      }
      String genericsParameters = "";
      if (type.generics.isNotEmpty) {
        genericsParameters = "<${type.generics.join(",")}>";
      }
      for (var constructor in type.constructors) {
        if (constructor.isAbstract) {
          continue;
        }
        String functionName = type.name;
        if (config.shortName) {
          functionName = functionName.removeIf(parentType.name);
        }
        functionName = functionName.firstToLower;
        var className = type.name;
        var returnType = constructor.returnType;

        if (constructor.name.isNotEmpty) {
          functionName = functionName + constructor.name.firstToUpper;
          className = className + "." + constructor.name;
        }

        final textConstructor = constructor.parameters.map(
              (parameter) {
            if (parameter.isNamed) {
              return parameter.name + " : " + parameter.name;
            } else {
              return parameter.name;
            }
          },
        ).join(" , ");
        var parameters = "";
        var hasOptional = false;
        var hasNamed = false;
        for (var parameter in constructor.parameters) {
          if (!hasOptional && (parameter.isOptional || parameter.isNamed)) {
            hasOptional = true;
            if (parameter.isNamed) {
              hasNamed = true;
              parameters += "{";
            } else {
              parameters += "[";
            }
          }
          if (hasNamed && !parameter.isOptional) {
            parameters += "required ";
          }
          if (parameter.importPrefix != null) {
            parameters += "${parameter.importPrefix}.";
          }
          parameters += parameter.type;
          parameters += " ";
          parameters += parameter.name;
          if (parameter.defaultValueCode != null) {
            parameters += "= ${parameter.defaultValueCode}";
          }
          parameters += ", ";
        }
        if (hasOptional) {
          if (hasNamed) {
            parameters += "}";
          } else {
            parameters += "]";
          }
        }
        final function = """
        static $returnType $functionName$genericsParameters($parameters){
          return $className($textConstructor);
        } 
        """;

        methods.add(function);
      }
    }
    return methods.join("\n");
  }

  String? getImportPrefix(ParameterElement parameter,) {
    final importIdentifier = parameter.type.element?.library?.identifier;
    if (importIdentifier == null) {
      return null;
    }
    final import = parameter.library!.imports.firstWhereOrNull(
          (element) => element.importedLibrary!.identifier == importIdentifier,
    );
    if (import?.prefix != null) {
      return import!.prefix!.name;
    }
    return null;
  }
}
