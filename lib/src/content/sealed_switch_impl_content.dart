import 'package:like_sealed/like_sealed.dart';
import 'package:like_sealed_gen/src/description/class_description.dart';
import 'package:like_sealed_gen/src/util.dart';

class SealedSwitchImplContent {
  final LikeSealed config;
  final ClassDescription parentType;
  final List<ClassDescription> types;
  final bool nullSafety;

  SealedSwitchImplContent(
    this.config,
    this.parentType,
    this.types,
    this.nullSafety,
  );

  String content() {
    String typeChecker = "";
    String field = "";
    String fillConstructorParameter = "";
    List<String> constructorParameter = [];
    List<String> requiredConstructorSuper = [];
    String caseFunction = "";
    final hasGenerics = parentType.generics.isNotEmpty;

    final inputType = hasGenerics ? "I" : parentType.name;

    for (var type in types) {
      final typeName = type.name;
      String functionName = typeName;
      if (config.shortName) {
        functionName = functionName.removeIf(parentType.name);
      }
      final filedName = functionName.isEmpty
          ? parentType.name.firstToLower
          : functionName.firstToLower;

      requiredConstructorSuper.add("_${filedName}=${filedName}");
      fillConstructorParameter += "this._${filedName},";
  final nullable=nullSafety?"?":"";
      final ignoreNull=nullSafety?"!":"";
      if (typeName == parentType.name) {
        constructorParameter.add("O Function($inputType)$nullable ${filedName}");
        field += "final O Function($inputType)$nullable  _${filedName};";
        typeChecker +=
            "(type is $inputType&& _${filedName} != null) ? onDefault(type) : throw UnimplementedError()";

        caseFunction +=
            "O onDefault($inputType value)=> _${filedName}$ignoreNull(value);";
      } else {
        constructorParameter.add("O Function($typeName)$nullable ${filedName}");
        field += "final O Function($typeName)$nullable  _${filedName};";
        typeChecker +=
            "(type is $typeName && _${filedName} != null) ? on$functionName(type) : ";

        caseFunction +=
            "O on$functionName($typeName value)=> _${filedName}$ignoreNull(value) ;";
      }
    }

    var requiredConstructorParameterString = "";
    for (var value in constructorParameter) {
      requiredConstructorParameterString += "$value,";
    }
    var constructorParameterString = "";
    for (var value in constructorParameter) {
      constructorParameterString += "${nullSafety?"":"@"}required $value,";
    }

    return """
    class ${parentType.name}SwitchImpl<${hasGenerics ? "I extends ${parentType.name}, " : ""}O> implements ${parentType.name}Switch<${hasGenerics ? "I," : ""}O> {
      ${field}
      
      ${parentType.name}SwitchImpl({${requiredConstructorParameterString}}): ${requiredConstructorSuper.join(",")};
      
      @Deprecated("use base constructor or required")
      ${parentType.name}SwitchImpl.fill($fillConstructorParameter);
      
       ${parentType.name}SwitchImpl.required({${constructorParameterString}}): ${requiredConstructorSuper.join(",")};
     
      O switchCase($inputType type)=>$typeChecker;
      
      $caseFunction
    }
    """;
  }
}
