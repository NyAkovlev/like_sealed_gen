import 'package:like_sealed/like_sealed.dart';
import 'package:like_sealed_gen/src/content/sealed_collection_content.dart';
import 'package:like_sealed_gen/src/content/sealed_switch_content.dart';
import 'package:like_sealed_gen/src/content/sealed_switch_impl_content.dart';
import 'package:like_sealed_gen/src/description/class_description.dart';

class ContentFormat {
  final LikeSealed config;
  final ClassDescription parentType;
  final List<ClassDescription> types;
  final bool nullSafety;

  ContentFormat(this.config, this.parentType, this.types, this.nullSafety);

  String build() {
    var content = "";
    if (config.collection) {
      content += SealedCollectionContent(
        config,
        parentType,
        types,
      ).content();
    }
    if (config.switchInterface) {
      content += SealedSwitchContent(
        config,
        parentType,
        types,
      ).content();
    }
    if (config.switchImpl) {
      content += SealedSwitchImplContent(
        config,
        parentType,
        types,
        nullSafety,
      ).content();
    }
    return content;
  }
}
